--create table dbo.teste_feriados ( id_feriado int primary key identity (1,1)
--                                , dia        smalldatetime not null
--								, nome_feriado varchar(10) not null
--								)
--insert into dbo.teste_feriados (dia, nome_feriado) values ('01/01/2021', 'Ano novo');
--insert into dbo.teste_feriados (dia, nome_feriado) values ('25/12/2021', 'Natal');
--insert into dbo.teste_feriados (dia, nome_feriado) values ('01/01/2022', 'ano novo a');


declare @dt_data_compra         smalldatetime = '25/12/2021' --variavel
      , @dv_feriado             bit           = 1 --fixo
	  , @id_dias_uteis          int           = 5 --variavel
	  , @id_fator_multiplicacao int           = 1 --fixo

if @id_dias_uteis > 5
begin
	/*descobrindo quantas semanas*/
	select @id_fator_multiplicacao = @id_dias_uteis / 5
	set @id_dias_uteis = @id_dias_uteis + (2 * @id_fator_multiplicacao) 
end

while @dv_feriado = 1
begin
	if exists(select top 1 1 from dbo.teste_feriados where dia = @dt_data_compra)
	begin
		select @dt_data_compra = dateadd(day, +1, @dt_data_compra) -- se cair no feriado passa pro dia seguinte
		select @dt_data_compra = (select case when datepart(weekday, @dt_data_compra) = 1 --domingo 
                                               then dateadd(day, +1, @dt_data_compra) -- +1 pra chegar na segunda
		                                  	  when datepart(weekday, @dt_data_compra) = 7 --sabado
						                 	   then dateadd(day, +2, @dt_data_compra) -- +2 pra chegar na segunda
						                 	  else @dt_data_compra -- dia de semana
	                                     end)
	end
	else
	begin
		select @dv_feriado = 0
	end
end

/*contar dias uteis a partir da data da compra*/
if (select datepart(weekday, dateadd(day, +@id_dias_uteis, @dt_data_compra))) = 1 --domingo
begin 
	set @id_dias_uteis = @id_dias_uteis + 1
end 

if (select datepart(weekday, dateadd(day, +@id_dias_uteis, @dt_data_compra))) = 7 -- sabado
begin 
	set @id_dias_uteis = @id_dias_uteis + 2
end

select dateadd(day, +@id_dias_uteis, @dt_data_compra)
